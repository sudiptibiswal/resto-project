import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 loginForm!:FormGroup;
  constructor(private FormBuilder:FormBuilder , private _http:HttpClient , private router:Router) { }

  ngOnInit(): void {
    this.loginForm=this.FormBuilder.group({
      email:[''],
      password:['']
    })

  }
  //login method here
  login(){
    this._http.get<any>("http://localhost:3000/singup").subscribe(res=>{
     const User= res.find((a:any)=>{
      return a.email === this.loginForm.value.email && a.password === this.loginForm.value.password
     })
     if(User){
       this.loginForm.reset()
       this.router.navigate(['resturant'])
     }else{
       alert("u put something worng data (or) u dont have account so plz singup before u login")
     }
    },
    err=>{
      alert ("server problem")
    }
    )
  }

}
function problem(server: any, problem: any) {
  throw new Error('Function not implemented.');
}

