import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApiService } from '../shared/api.service';
import { restroDashData } from './restro-dash.model';

@Component({
  selector: 'app-restro-dash',
  templateUrl: './restro-dash.component.html',
  styleUrls: ['./restro-dash.component.css']
})
export class RestroDashComponent implements OnInit {
  title='Resturent Record App';
  formValue!: FormGroup;

  resturentModelObj: restroDashData = new restroDashData;
  allResturentData: any;

  constructor(private FormBuilder: FormBuilder, private api: ApiService) { }

  ngOnInit(): void {
    this.formValue = this.FormBuilder.group({
      name: [''],
      email: [''],
      mobile: [''],
      address: [''],
      services: ['']
    })
    this.getAddresturentData();
  }


  //Now SubScribe our Data which is maped in services..0
  // And also post data 
  addResturent() {
    this.resturentModelObj.name = this.formValue.value.name;
    this.resturentModelObj.email = this.formValue.value.email;
    this.resturentModelObj.mobile = this.formValue.value.mobile;
    this.resturentModelObj.address = this.formValue.value.address;
    this.resturentModelObj.services = this.formValue.value.services;

    this.api.postResturent(this.resturentModelObj).subscribe((res: any) => {
      console.log("res", res);
      //to remove field of inputs
      this.formValue.reset()
      //to get data instant in our page
      this.getAddresturentData();
    },
      (err: any) => {
        alert("error when u post restro data plz check")
      }
    )
  }

  //get all data 
  getAddresturentData() {
    this.api.getResturent().subscribe((res: any) => {
      this.allResturentData = res;
    },
      (err: any) => {
        alert("error in getting data plz check")
      }
    )
  }

  //delet restrurent data if u want in one click  
  deleteResturentData(data: any) {
    this.api.deleteResturent(data.id).subscribe((res: any) => {
      alert("Are You sure to delete this resturant detail")
      //kuch delete karne ke bad tabhi usko delete karke baki sab data show karene ke liye without refresh page
      this.getAddresturentData();

    },
      (err: any) => {
        alert("error when u delet data plz check")
      }
    )
  }

  //showing btn dynamically
  showAdd!: boolean;
  showBtn!: boolean;

  ClickAddrestro() {
    this.formValue.reset();
    this.showAdd = true;
    this.showBtn = false;

  }
  
  //edit resto data
  onEditResturentData(data: any) {
    this.showAdd = false;
    this.showBtn = true;
    this.resturentModelObj.id = data.id;
    this.formValue.controls['name'].setValue(data.name);
    this.formValue.controls['email'].setValue(data.email);
    this.formValue.controls['mobile'].setValue(data.mobile);
    this.formValue.controls['address'].setValue(data.address);
    this.formValue.controls['services'].setValue(data.services);
  }

  //for update resturent data
  updataeResturent() {
    this.resturentModelObj.name = this.formValue.value.name;
    this.resturentModelObj.email = this.formValue.value.email;
    this.resturentModelObj.mobile = this.formValue.value.mobile;
    this.resturentModelObj.address = this.formValue.value.address;
    this.resturentModelObj.services = this.formValue.value.services;
    this.api.updateResturent(this.resturentModelObj, this.resturentModelObj.id).subscribe((res: any) => {
      alert("sucessfully you update you resturent data");
      this.formValue.reset();
      this.getAddresturentData()
    },
      (err: any) => {
        alert("error in updating data plz check")
      }
    )
  }

}


