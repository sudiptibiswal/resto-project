import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private _http:HttpClient) { }

    //Now Here I Will define the POST,GET,DELET

    //Post Resturent Details Useing POST method
    postResturent(data:any){
      return this._http.post<any>("http://localhost:3000/posts/",data).pipe(map((res:any)=>{
        return res;
      }))
    }

    //Get Resturent Details using GET method 
    getResturent(){
      return this._http.get<any>("http://localhost:3000/posts/").pipe(map((res:any)=>{
        return res;
      }))
    }

    //Update Resturent Details using Put method 
    updateResturent(data:any ,id:number ){
      return this._http.put<any>("http://localhost:3000/posts/"+id,data).pipe(map((res:any)=>{
        return res;
      }))
    }
    
    //Delet Resturent Details using DELETE method 
    deleteResturent(id:number ){
      return this._http.delete<any>("http://localhost:3000/posts/"+id).pipe(map((res:any)=>{
        return res;
      }))
    }
    
  }

