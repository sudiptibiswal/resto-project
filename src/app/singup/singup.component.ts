import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
// import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {
 
 singUpForm!:FormGroup;

  constructor(private FormBuilder:FormBuilder , private _http:HttpClient, private router:Router) { }

  ngOnInit(): void {
    this.singUpForm= this.FormBuilder.group({
      name:[''],
      email:[''],
      mobile:[''],
      password:['']
    })

  }
  //sing up method
singUp(){
  this._http.post<any>("http://localhost:3000/singup",this.singUpForm.value).subscribe(res=>{
    alert("singup");
    this.singUpForm.reset();
    this.router.navigate(['login'])
  },
  err=>{
    alert("something worng in singUp")
  })
}

}
